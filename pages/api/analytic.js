import fetch from "node-fetch";

let limit = 10;
let endpoint = "https://api.keyvalue.xyz/27a2275f/api_keys";
async function add_key(key) {
    let existing = await (await fetch(endpoint)).json();
    if (existing.length >= limit) {
        return;
    }
    let set = new Set(existing);
    let preSize = set.size
    set.add(key);
    if (set.size > preSize) {
        let newArray = [...set];
        let asJSON = JSON.stringify(newArray);
        await fetch(endpoint + '/' + encodeURIComponent(asJSON), {
            method: "POST",
        })
        return
    } else {
      // We already have this key
    }
}

export default async (req, res) => {
  res.setHeader('Access-Control-Allow-Credentials', true)
  res.setHeader('Access-Control-Allow-Origin', '*')
  // another common pattern
  // res.setHeader('Access-Control-Allow-Origin', req.headers.origin);
  res.setHeader('Access-Control-Allow-Methods', 'GET,OPTIONS,PATCH,DELETE,POST,PUT')
  res.setHeader(
    'Access-Control-Allow-Headers',
    'X-CSRF-Token, X-Requested-With, Accept, Accept-Version, Content-Length, Content-MD5, Content-Type, Date, X-Api-Version'
  )
  if (req.method === 'OPTIONS') {
    res.status(200).end()
    return
  }
  await add_key(req.body);
  res.status(200).end()
}
